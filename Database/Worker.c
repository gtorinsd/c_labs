#include <stdlib.h>
#include <stdio.h>
#include "DBworker.h"
#include "Tools.h"

#define FileName "out.txt"

void ShowAllRecords(SDataTable table)
{
    int i;
    for(i = 0; i < table.RecordCount; i++)
    {
        ShowRecord(table.Records[i]);
    }
}

void FillAllRecords(SDataTable table)
{
    int i;
    for(i = 0; i < table.RecordCount; i++)
    {
        FillByValue(&table.Records[i]);
    }
}

void GetAllRecordsFromKeyboard(SDataTable table)
{
    int i;
    for(i = 0; i < table.RecordCount; i++)
    {
        printf("Record #%d\n", i);
        GetFromKeyboard(&table.Records[i]);
    }
}

void Work()
{
    puts("Begin work");

    printf("Input records count: ");
    int recCount = GetIntFromKeyboard();

    SDataTable table = InitTable(recCount);
    //FillAllRecords(table);
    //GetAllRecordsFromKeyboard(table);
    //SaveDataTableToFile(table, FileName);

    LoadDataTableFromFile(&table, FileName);
    ShowAllRecords(table);

    // Get record by index
    /*
    int recNo;
    printf("\nInput record # for display: ");
    recNo = GetIntFromKeyboard() - 1;
    SDataRecord record = GetRecordByNumber(table,  recNo, FileName);
    printf("Looking for record #%d\n", recNo);
    ShowRecord(record);
    */

    // Find method #1: record by Name
    printf("\nInput Name for search: ");
    char *Name = malloc(STRINGLENGTH);
    gets(Name);
    printf("Looking for Name = %s\n", Name);
    SDataRecord record = FindRecordByName(table, Name);
    ShowRecord(record);

    //  Find method #2: record by Prise
    /*
    printf("\nInput Prise value for search: ");
    float Prise = GetFloatFromKeyboard();
    printf("Looking for Prise = %f\n", Prise);
    SDataRecord record = FindRecordByPrise(table, Prise);
    ShowRecord(record);
    */

    puts("\nDone.");
}
