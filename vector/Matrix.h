#ifndef __MATRIX_H
#define __MATRIX_H
#include "SMatrix.h"

SVector VectorCreate(unsigned long int);

void VectorFillByIndex(SVector*);
void VectorFillRandomly(SVector*);
void VectorFillFromKeyboard(SVectorPtr*);
void VectorLoadFromFile(SVector *v, const char *fileName);
void VectorSaveToFile(SVector v1, const char *fileName);
void VectorDisplay(SVector);

SVector VectorAdd(SVector,SVector);
SVector VectorDiff(SVector,SVector);
double VectorScalar(SVector,SVector);
SVector VectorMultConst(SVector,double);
SVector VectorCopy(SVector);
void VectorDelete(SVector*);

SMatrix MatrixCreate(unsigned long int,unsigned long int);
void MatrixFillRandomly(SMatrixPtr *);
void MatrixFillFromKeyboard(SMatrixPtr *);
void MarixLoadFromFile(SMatrixPtr *m, const char *fileName);
void MarixSaveToFile(SMatrix m, const char *fileName);
void MatrixDisplay(SMatrix);

void MatrixMakeE(SMatrix* );
SMatrix MatrixMMMult(SMatrix,SMatrix);
SMatrix MatrixMMAdd(SMatrix,SMatrix);
SMatrix MatrixMVMult(SMatrix,SVector);
SMatrix MatrixCopy(SMatrix);
void MatrixDelete(SMatrix*);

#endif
