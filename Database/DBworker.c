#include <stdlib.h>
#include <stdio.h>
#include "DBworker.h"
#include "Tools.h"

SDataRecord InitRecord()
{
    SDataRecord ac;

    ac.Name = calloc(STRINGLENGTH, 1);
    ac.Code = calloc(STRINGLENGTH, 1);
    ac.Type = calloc(STRINGLENGTH, 1);
    ac.Developer = calloc(STRINGLENGTH, 1);

    ac.Name = "";
    ac.Code = "";
    ac.Type = "";
    ac.Prise = 0;
    ac.Developer = "";
    ac.Warranty = 0;

    return ac;
}

SDataTable InitTable(const int recCount)
{
    SDataRecord *records = calloc(recCount,sizeof(SDataRecord));
    int i;
    for(i = 0; i < recCount; i++)
        records[i] = InitRecord();

    SDataTable table;
    table.RecordCount = recCount;
    table.Records = records;

    return table;
}

void ShowRecord(SDataRecord ac)
{
    //printf("%s %s %s %5.2f %s %d\n", ac.Name, ac.Code, ac.Type, ac.Prise, ac.Developer, ac.Warranty);
    printf("%s\t%s\t%s\t%5.2f\t%s\t%d\n", ac.Name, ac.Code, ac.Type, ac.Prise, ac.Developer, ac.Warranty);
}

void FillByValue(SDataRecordPtr *ac)
{
    (*ac).Name = "TestName";
    (*ac).Code = "TestCode";
    (*ac).Type = "TestType";
    (*ac).Prise = 1.23;
    (*ac).Developer = "TestDeveloper";
    (*ac).Warranty = 5;
}

void GetFromKeyboard(SDataRecordPtr *ac)
{
    printf("Input Name: ");
    (*ac).Name = GetStringFromKeyboard(STRINGLENGTH);

    printf("Input code: ");
    (*ac).Code = GetStringFromKeyboard(STRINGLENGTH);

    printf("Input Type: ");
    (*ac).Type = GetStringFromKeyboard(STRINGLENGTH);

    printf("Input prise: ");
    (*ac).Prise = GetFloatFromKeyboard();

    printf("Input developer: ");
    (*ac).Developer = GetStringFromKeyboard(STRINGLENGTH);

    printf("Input warranty: ");
    (*ac).Warranty = GetIntFromKeyboard();
}

int GetRecordFromFile(SDataRecordPtr *Record , FILE *f)
{
    char *Name = malloc(STRINGLENGTH);
    char *Code = malloc(STRINGLENGTH);
    char *Type = malloc(STRINGLENGTH);
    float Prise;
    char *Developer = malloc(STRINGLENGTH);
    int Warranty;

    int res = fscanf(f, "%s\n%s\n%s\n%f\n%s\n%d\n",
            Name,
            Code,
            Type,
            &Prise,
            Developer,
            &Warranty
            );

    (*Record).Name = CopyString(Name, STRINGLENGTH);
    (*Record).Code = CopyString(Code, STRINGLENGTH);
    (*Record).Type = CopyString(Type, STRINGLENGTH);
    (*Record).Prise = Prise;
    (*Record).Developer = CopyString(Developer, STRINGLENGTH);
    (*Record).Warranty = Warranty;
    return res;
}

void LoadDataTableFromFile(SDataTable *table, const char *fileName)
{
    FILE *file = fopen(fileName, "r");
    if (file == 0)
    {
        printf("Unable to open file: %s\n", fileName);
        return;
    }
    int i;
    for(i = 0; i < (*table).RecordCount; i++)
    {
        SDataRecordPtr record = InitRecord();
        int res = GetRecordFromFile(&record, file);

        if (res == EOF)
        {
            break;
        }

        (*table).Records[i] = record;
    }

    fclose(file);
}

void SaveDataTableToFile(SDataTable table, const char *fileName)
{
    FILE *file = fopen(fileName, "w+");
    if (file == 0)
    {
        printf("Unable to open file: %s\n", fileName);
        return;
    }

    int i;
    for(i = 0; i < table.RecordCount; i++)
    {
        fprintf(file, "%s\n%s\n%s\n%f\n%s\n%d\n",
                table.Records[i].Name,
                table.Records[i].Code,
                table.Records[i].Type,
                table.Records[i].Prise,
                table.Records[i].Developer,
                table.Records[i].Warranty
                );
    }

    fclose(file);
}

SDataRecord GetRecordByNumber(SDataTable table, const int RecordNumber, const char *fileName)
{
    SDataRecord record = InitRecord();
    if (RecordNumber <= table.RecordCount)
    {
        record = table.Records[RecordNumber];
    }

    return record;
}

SDataRecord FindRecordByName(SDataTable table, const char *Name)
{
    int i;
    for(i = 0; i < table.RecordCount; i++)
    {
        // case insensitive comparator
        if (strcmpi(table.Records[i].Name, Name) == 0)
        {
            return table.Records[i];
        }
    }
    // Not found :(
    return InitRecord();
}

SDataRecord FindRecordByPrise(SDataTable table, const int Prise)
{
    int i;
    for(i = 0; i < table.RecordCount; i++)
    {
        if (table.Records[i].Prise == Prise)
        {
            return table.Records[i];
        }
    }
    // Not found :(
    return InitRecord();
}
