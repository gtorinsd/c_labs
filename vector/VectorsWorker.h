#ifndef VECTORSWORKER_H_INCLUDED
#define VECTORSWORKER_H_INCLUDED
#include "Matrix.h"

void FillVectorAutomatically(SVector v, char *FileName);
void VectorsWork(int n, char *FileName);

#endif // VECTORSWORKER_H_INCLUDED
