#ifndef MATRIXWORKER_H_INCLUDED
#define MATRIXWORKER_H_INCLUDED
#include "Matrix.h"

void FillMatrixAutomatically(SMatrix m, char *FileName);
void MatrixWork(int col, int row, char *FileName);

#endif // MATRIXWORKER_H_INCLUDED
