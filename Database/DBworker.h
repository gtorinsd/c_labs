#ifndef ACCESSORIES_H_INCLUDED
#define ACCESSORIES_H_INCLUDED

#define STRINGLENGTH 5

typedef struct PCDataRecord
{
    char *Name;
    char *Code;
    char *Type;
    float Prise;
    char *Developer;
    int Warranty;
    // text field length is STRINGLENGTH
} SDataRecord;
typedef SDataRecord SDataRecordPtr;

typedef struct PCDataTable
{
   int RecordCount;
   SDataRecordPtr *Records;
} SDataTable;
typedef SDataTable SDataTablePtr;

SDataRecordPtr InitRecord();
SDataTable InitTable(const int);
void ShowRecord(SDataRecord);
void FillByValue(SDataRecordPtr *);
void GetFromKeyboard(SDataRecordPtr *);

void LoadDataTableFromFile(SDataTable *, const char *);
void SaveDataTableToFile(SDataTable, const char *);
SDataRecord GetRecordByNumber(SDataTable, const int, const char *);
SDataRecord FindRecordByName(SDataTable, const char *);
SDataRecord FindRecordByPrise(SDataTable, const int);

// There is not the strcmpi in the <strings.h> header file
// It's need to remove the implicit declaration of function 'strcmpi' warning
int strcmpi(const char*, const char*);

#endif // ACCESSORIES_H_INCLUDED
