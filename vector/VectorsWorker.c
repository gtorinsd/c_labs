#include <stdlib.h>
#include <stdio.h>
#include "VectorsWorker.h"

/// Fill vector by values, 2 examples.
/// Using pointer to function demo
void FillVectorAutomatically(SVector v, char *FileName)
{
    // Declare the function pointer
    void (*VectorFill)(SVector*) = NULL;

    // Fill vector by ordered values from index
    //VectorFill = VectorFillByIndex;

    // Fill vector by random values
    VectorFill = VectorFillRandomly;

    // Call the function
    VectorFill(&v);
}

void VectorsWork(int n, char *FileName)
{
    SVector v = VectorCreate(n);

    // Fill vector by values automatically
    FillVectorAutomatically(v, FileName);
    VectorDisplay(v);
    VectorSaveToFile(v, FileName);

    // Fill vector from keyboard
    VectorFillFromKeyboard(&v);
    VectorDisplay(v);
    VectorSaveToFile(v, FileName);

    // Fill vector from file
    VectorLoadFromFile(&v, FileName);
    VectorDisplay(v);
}
