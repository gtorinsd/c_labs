#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Matrix.h"
#include "Tools.h"

SVector VectorCreate(unsigned long int NumElement)
{
    SVector v;
    v.VectorSize=NumElement;
    v.Data=calloc(NumElement,sizeof(double));
    return v;
}

void VectorFillByIndex(SVectorPtr *v1)
{
    int i;
    printf("\nsize of vector is %d\n", (*v1).VectorSize);
    for(i=0;i<(*v1).VectorSize;i++)
    {
        (*v1).Data[i] = i + 1;
    }
}

void VectorFillRandomly(SVectorPtr *v1)
{
    int i;
    printf("\nsize of vector is %d\n", (*v1).VectorSize);
    srand(time(NULL));
    for(i=0;i<(*v1).VectorSize;i++)
    {
        (*v1).Data[i] = GetRandomValue(9);
    }
}

void VectorFillFromKeyboard(SVectorPtr *v1)
{
    int i;
    for(i=0;i<(*v1).VectorSize;i++)
    {
        printf("Input v[%d]: ", i);
        int res, value;
        do
          {
            res=scanf("%d", &value);
            fflush(stdin);
            if(res!=1)
            {
                printf("Invalid input. Try again: ");
            }
          }
        while(res!=1);
        (*v1).Data[i] = value;
    }
}

void VectorLoadFromFile(SVectorPtr *v, const char *fileName)
{

    FILE *file = fopen(fileName, "r");
    // Check if file exists
    if (file == 0)
    {
        printf("Unable to open file: %s\n", fileName);
        return;
    }

    int i = 0;
    int res, value;
    while(res = fscanf(file, "%d\n", &value) != EOF)
    {
        (*v).Data[i] = value;
        i++;
        if (i>= (*v).VectorSize)
        {
            break;
        }
    }
    fclose(file);
}

void VectorSaveToFile(SVector v1, const char *fileName)
{
    FILE *file = fopen(fileName, "w+");
    int i;
    for(i = 0; i < v1.VectorSize;i++)
    {
        fprintf(file, "%g\n", v1.Data[i]);
    }

    fclose(file);
}

void VectorDisplay(SVector v1)
{
    int i;
    for(i=0;i<v1.VectorSize;i++)
    {
        printf(" %g", v1.Data[i]);
    }
    puts("");
}


SVector VectorAdd(SVector v1,SVector v2)
{

}
SVector VectorDiff(SVector v1,SVector v2)
{

}
double VectorScalar(SVector v1,SVector v2)
{

}
SVector VectorMultConst(SVector v1,double c)
{

}
SVector VectorCopy(SVector v1)
{

}
void VectorDelete(SVector *v1)
{

}


// Matrix
SMatrix MatrixCreate(unsigned long int row, unsigned long int col)
{
    SMatrix m;
    m.Col = col;
    m.Row = row;

    m.Data = calloc(row,sizeof(double));
    int i =0;
    for(i = 0; i < m.Col; i++)
    {
        m.Data[i] = calloc(m.Row,sizeof(double));
    }

    return m;
}

void MatrixFillRandomly(SMatrixPtr *m)
{
    int i, j;
    srand(time(NULL));
    for(i=0; i<(*m).Col; i++)
    {
        for(j = 0; j<(*m).Row; j++)
        {
            int num = GetRandomValue(9);
            (*m).Data[i][j] = num;
        }
    }
}

void MatrixFillFromKeyboard(SMatrixPtr *m)
{
    int i, j;
    for(i=0; i<(*m).Col;i++)
    {
        for(j=0; j<(*m).Row; j++)
        {
            printf("Input m[%d, %d]: ", i, j);
            int res, value;
            do
              {
                res=scanf("%d", &value);
                fflush(stdin);
                if(res!=1)
                {
                    printf("Invalid input. Try again: ");
                }
              }
            while(res!=1);
            (*m).Data[i][j] = value;
        }
    }
}

void MarixLoadFromFile(SMatrixPtr *m, const char *fileName)
{
    FILE *file = fopen(fileName, "r");
    // Check if file exists
    if (file == 0)
    {
        printf("Unable to open file: %s\n", fileName);
        return;
    }

    int i = 0, j = 0;
    int res, value;
    while(res = fscanf(file, "%d\n", &value) != EOF)
    {
        (*m).Data[i][j] = value;
        printf("m[%d, %d] = %g\n", i, j, (*m).Data[i][j]);

        if (j + 1 < (*m).Row)
        {
            j++;
        }
        else
        {
            i++;
            j = 0;
        }

        if (i >= (*m).Col)
        {
            break;
        }
    }
    fclose(file);
}

void MarixSaveToFile(SMatrix m, const char *fileName)
{
    FILE *file = fopen(fileName, "w+");
    int i, j;
    for(i = 0; i < m.Col; i++)
    {
        for(j = 0; j < m.Row; j++)
        fprintf(file, "%g\n", m.Data[i][j]);
    }
    fclose(file);
}

void MatrixDisplay(SMatrix m)
{
    int i, j;
    for(i = 0; i < m.Col; i++)
    {
        for(j = 0; j < m.Row; j++)
        {
            printf(" %g", m.Data[i][j]);
        }
        puts("");
    }
}

void MatrixMakeE(SMatrix *m1)
{

}
SMatrix MatrixMMMult(SMatrix m1,SMatrix m2)
{

}
SMatrix MatrixMMAdd(SMatrix m1,SMatrix m2)
{

}

SMatrix MatrixMVMult(SMatrix m1,SVector v1)
{

}
SMatrix MatrixCopy(SMatrix m1)
{

}

void MatrixDelete(SMatrix *m1)
{

}
