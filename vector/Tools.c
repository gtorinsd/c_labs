#include <stdlib.h>
#include <time.h>

int GetRandomValueMinMax(int minValue, int maxValue)
{
        return rand() % (maxValue + 1 - minValue) + minValue;
}

int GetRandomValue(int maxValue)
{
       return GetRandomValueMinMax(0, maxValue);
}

