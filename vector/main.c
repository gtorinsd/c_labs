#include <stdio.h>
#include "matrix.h"
#include "VectorsWorker.h"
#include "MatrixWorker.h"

#define n 5
#define m 4
#define FileName "out.txt"

/// ==============
/// Main function
/// ==============
int main()
{
    printf("\nStart!\n");

    // Vector
    VectorsWork(n, FileName);

    // matrix
    MatrixWork(m, n, FileName);

    printf("\nStoped!\n");
    return 0;
}
