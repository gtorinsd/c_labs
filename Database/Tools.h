#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

char* GetStringFromKeyboard(const int length);
float GetFloatFromKeyboard();
int GetIntFromKeyboard();
char* CopyString(const char *, const int);

#endif // TOOLS_H_INCLUDED
