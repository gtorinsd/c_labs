#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Tools.h"

char* GetStringFromKeyboard(const int length)
{
    char *strP = malloc(length);
    fgets (strP, length, stdin);
    fflush(stdin);

    // remove \n from the input string
    strP[strcspn(strP, "\r\n")] = 0;

    return strP;
}

float GetFloatFromKeyboard()
{
    int res;
    float value;
    do
    {
        res = scanf("%f", &value);
        fflush(stdin);
        if((res != 1) || (value <= 0))
        {
            printf("Invalid input. Try again: ");
            // value <= 0
            res = -1;
        }
    }
    while(res != 1);
    return value;
}

int GetIntFromKeyboard()
{
    int res, value;
    do
    {
        res = scanf("%d", &value);
        fflush(stdin);
        if((res != 1) || (value <= 0))
        {
            printf("Invalid input. Try again: ");
            // value <= 0
            res = -1;
        }
    }
    while(res !=1 );
    return value;
}

char* CopyString(const char *strIn, const int length)
{
    char *strDest = malloc(length);
    strncpy(strDest, strIn, length);
    return strDest;
}
