#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

int GetRandomValueMinMax(int minValue, int maxValue);
int GetRandomValue(int maxValue);

#endif // TOOLS_H_INCLUDED
