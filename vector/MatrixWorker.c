#include <stdlib.h>
#include <stdio.h>
#include "MatrixWorker.h"
#include "Matrix.h"

/// Fill matrix by values
void FillMatrixAutomatically(SMatrix m, char *FileName)
{
    // Declare the function pointer
    void (*MatrixFill)(SMatrix*) = NULL;

    // Fill matrix by ordered values from index
    //MatrixFill = MatrixFillByIndex;

    // Fill matrix by random values
    MatrixFill = MatrixFillRandomly;
    // Call the function
    MatrixFill(&m);
}

void MatrixWork(int col, int row, char *FileName)
{
    SMatrix m = MatrixCreate(row, col);

    // Fill matrix by values automatically
    //FillMatrixAutomatically(m, FileName);
    //MatrixDisplay(m);
    //MarixSaveToFile(m, FileName);

    // Fill matrix from keyboard
    //MatrixFillFromKeyboard(&m);
    //MatrixDisplay(m);
    //MarixSaveToFile(m, FileName);

    // Fill matrix from file
    MarixLoadFromFile(&m, FileName);
    MatrixDisplay(m);
}
